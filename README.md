# Gamigo NPM Packages

Repository for sourcing Gamigo NPM packages.

## Publishing

Once your GitLab account has received access to this repository:

1. Create a [personal access token](https://gitlab.com/-/profile/personal_access_tokens), with the scope set to `api`
2. Set your npm configuration:

    ```shell
    # Set URL for your scoped packages
    npm config set @gamigo:registry https://gitlab.com/api/v4/projects/25443101/packages/npm/

    # Add the token for the scoped packages URL
    npm config set -- '//gitlab.com/api/v4/projects/25443101/packages/npm/:_authToken' "<your_token>"
    ```

3. You should now be able to publish and install npm packages in your project. Run `npm publish` to publish your package.

You can also use `yarn` instead of `npm` if preferable.
